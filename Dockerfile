ARG DEBIAN_VERSION
ARG RUST_VERSION

FROM rust:${RUST_VERSION}-slim-${DEBIAN_VERSION} AS builder

ARG RLS_VERSION

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        curl \
        tar && \
    curl -fLo rust-analyzer-${RLS_VERSION}.tar.gz \
        https://github.com/rust-lang/rust-analyzer/archive/${RLS_VERSION}/rust-analyzer-${RLS_VERSION}.tar.gz && \
    tar xzf rust-analyzer-${RLS_VERSION}.tar.gz && \
    cd rust-analyzer-${RLS_VERSION} && \
    cargo build --release --manifest-path crates/rust-analyzer/Cargo.toml && \
    mkdir -p /rust-analyzer/bin/ && \
    cp target/release/rust-analyzer /rust-analyzer/bin/


FROM rust:${RUST_VERSION}-slim-${DEBIAN_VERSION}

COPY --from=builder /rust-analyzer/bin/ /usr/local/bin/
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ncat

CMD ["rust-analyzer"]
